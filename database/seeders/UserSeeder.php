<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Gerencia;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permission::truncate();
        // Role::truncate();
        // User::truncate();

        $adminRole = Role::create(['name' => 'admin', 'display_name' => 'Administrador']);
        $operadorRole = Role::create(['name' => 'operador', 'display_name' => 'Operador']);

        Permission::create(['name' => 'ver-usuario', 'display_name' => 'Ver usuarios']);
        Permission::create(['name' => 'crear-usuario', 'display_name' => 'Crear usuarios']);
        Permission::create(['name' => 'actualizar-usuario', 'display_name' => 'Editar usuarios']);
        Permission::create(['name' => 'eliminar-usuario', 'display_name' => 'Eliminar usuarios']);

        Permission::create(['name' => 'ver-role', 'display_name' => 'Ver roles']);
        Permission::create(['name' => 'crear-role', 'display_name' => 'Crear roles']);
        Permission::create(['name' => 'actualizar-role', 'display_name' => 'Editar roles']);
        Permission::create(['name' => 'eliminar-role', 'display_name' => 'Eliminar roles']);

        Permission::create(['name' => 'ver-permiso', 'display_name' => 'Ver permisos']);
        Permission::create(['name' => 'actualizar-permiso', 'display_name' => 'Editar permisos']);

        Permission::create(['name' => 'ver-tipo-cambio', 'display_name' => 'Ver tipo de cambios']);
        Permission::create(['name' => 'crear-tipo-cambio', 'display_name' => 'Crear tipo de cambios']);
        Permission::create(['name' => 'actualizar-tipo-cambio', 'display_name' => 'Editar tipo de cambios']);
        Permission::create(['name' => 'eliminar-tipo-cambio', 'display_name' => 'Eliminar tipo d cambios']);


        $admin = new User;
        $admin->name = 'Administrador';
        $admin->username = 'admin';
        $admin->email = 'admin@mail.com';
        $admin->password = '654321';
        $admin->save();
        $admin->assignRole($adminRole);

        $operador = new User;
        $operador->name = 'Operador';
        $operador->username = 'operador';
        $operador->email = 'operador@mail.com';
        $operador->save();
        $operador->assignRole($operadorRole);
    }
}
