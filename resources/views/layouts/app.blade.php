<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Sistema de tipo de cambio.">
  <meta name="author" content="TIPO DE CAMBIO">
  <title>@yield('title') | {{ ENV('APP_NAME') }}</title>
  <link rel="icon" type="image/png" href="{{ asset('icon.png') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <link rel="stylesheet" href="{{ asset('css/admin.min.css') }}">
  @stack('css')
</head>
<body>
    @if(request()->routeIs('admin.*'))
        @include('admin.partials.sidenav')
    @endif
    <div class="main-content" id="panel">
        @include('admin.partials.nav')
        <main>
            @yield('content')
        </main>
    </div>
    <script src="{{ asset('js/admin.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    @include('admin.partials.sessions.errors')
    @include('admin.partials.sessions.success')
    @stack('js')
    <script>
        @if(session()->has('success'))
            $(document).ready(function() {
                toastr.options = {
                    'closeButton': true,
                    'progressBar': true,
                    'positionClass': 'toast-bottom-right',
                    'timeOut': '15000',
                    'showEasing': 'swing',
                    'hideEasing': 'linear',
                    'showMethod': 'fadeIn',
                    'hideMethod': 'fadeOut',
                }
                toastr.success('<small>{{ session('success') }}</small>', '¡Perfecto!');
            });
        @endif
        @if($errors->any())
            $(document).ready(function() {
                toastr.options = {
                    'closeButton': true,
                    'progressBar': true,
                    'positionClass': 'toast-bottom-right',
                    'timeOut': '15000',
                    'showEasing': 'swing',
                    'hideEasing': 'linear',
                    'showMethod': 'fadeIn',
                    'hideMethod': 'fadeOut',
                }
            // @foreach($errors->all() as $error)
                toastr.error('<small>{{ $errors->first() }}</small>', '¡Error!');
            // @endforeach
            });
        @endif
    </script>
</body>
</html>
