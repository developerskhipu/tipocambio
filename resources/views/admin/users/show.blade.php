@extends('layouts.app')
@section('title', 'Perfil de usuario')
@section('content')
{{ Breadcrumbs::render('usuarios.ver', $user) }}
    <div class="container-fluid mt--6">
        <div class="row">
            {{--PERFIL--}}
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card card-profile">
                    <img height="80px" src="{{ asset('assets/img/fondos/fondo-01.jpg')}}" alt="Image placeholder" class="card-img-top">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                                @if($user->avatar != null)                        
                                    <img class="rounded-circle shadow-lg" alt="{{ $user->name }}" src="/storage/{{ $user->avatar }}">
                                @else
                                    <img class="rounded-circle shadow-lg" alt="{{ $user->name }}" src="{{ asset('assets/img/placeholder.jpg') }}">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-6">
                        <div class="text-center">
                            <h5 class="h3">{{ $user->name }}</h5>
                            <div class="h5 font-weight-300">
                                <i class="ni location_pin mr-2"></i>{{ $user->getRoleDisplayNames() }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="card-profile-stats my-0 py-0">
                                    <div class="row d-inline-block">
                                        <div class="col text-left">
                                            <span class="description">Email</span>
                                        </div>
                                        <div class="col">
                                        <span class="description text-default">
                                            <b>{{ $user->email }}</b>
                                        </span>
                                        </div>
                                    </div>
                                    <hr class="divider my-0">
                                    <div class="row d-inline-block">
                                        <div class="col text-left">
                                            <span class="description">Nombre de usuario</span>
                                        </div>
                                        <div class="col text-left">
                                        <span class="description text-default">
                                            <b>{{ $user->username }}</b>
                                        </span>
                                        </div>
                                    </div>
                                    @if($user->roles->count())
                                        <hr class="divider my-0">
                                        <div class="row d-inline-block">
                                            <div class="col text-left">
                                                <span class="description">Roles</span>
                                            </div>
                                            <div class="col text-left">
                                            <span class="description text-default">
                                                <b>{{ $user->getRoleDisplayNames() }}</b>
                                            </span>
                                            </div>
                                        </div>
                                    @endif
                                    <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-default btn-lg btn-block text-white">Editar</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{--ROLES--}}
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <!-- Title -->
                        <h5 class="h3 mb-0">Roles</h5>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <!-- List group -->
                        <ul class="list-group list-group-flush list my--3">
                            @forelse($user->roles as $role)
                                <li class="list-group-item px-0">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <p class="h4 mb-0">
                                                {{ $role->display_name }}
                                            </p>
                                            @if($role->permissions->count())
                                                <hr class="divider">
                                                <i class="ni ni-app"></i>
                                                <small>Permisos: {{ $role->permissions->pluck('display_name')->implode(', ') }}</small>
                                            @endif
                                            @unless($loop->last)
                                                <hr>
                                            @endunless
                                        </div>
                                    </div>
                                </li>
                            @empty
                                <small class="text-muted">No tiene roles</small>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
            {{--PERMISOS--}}
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <!-- Title -->
                        <h5 class="h3 mb-0">Permisos extra</h5>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <!-- List group -->
                        <ul class="list-group list-group-flush list my--3">
                            @forelse($user->permissions as $permission)
                            <small>
                                {{ $permission->display_name }}
                            </small>
                            @empty
                                <small class="text-muted">No tiene permisos adicionales</small>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection