@extends('layouts.app')
@section('title', 'Crear usuario')
@section('content')
{{ Breadcrumbs::render('usuarios.crear') }}
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-md-8 mr-auto ml-auto">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Crear Usuario</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        
                        <h6 class="heading-small text-muted mb-4">Información del Usuario</h6>
                        <form method="POST" action="{{ route('admin.users.store') }}">
                            @csrf @method('POST')
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Nombre</label>
                                            <input name="name" class="form-control" placeholder="Nombre" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="username">Usuario</label>
                                            <input name="username" class="form-control" placeholder="Nombre de usuario" value="{{ old('username') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">Email</label>
                                            <input name="email" class="form-control" placeholder="hola@email.com" value="{{ old('email') }}">
                                        </div>
                                    </div>                                    
                                    <div class="col-lg-6">
                                        <label class="form-control-label">Roles</label>
                                        @include('admin.roles.checkboxes')
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="form-control-label">Permisos</label>
                                        @include('admin.permissions.checkboxes', ['model' => $user])
                                    </div>
                                </div>
                                <div class="row my-4">
                                    <div class="col-lg-6 mr-auto ml-auto">
                                        <div class="form-group">
                                            <button class="btn btn-default btn-block">Crear usuario</button>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <span class="text-default h5">La contraseña generada por defecto es: 123456</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection