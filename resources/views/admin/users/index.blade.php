@extends('layouts.app')
@section('title', 'Listado de Usuarios')
@section('content')
{{ Breadcrumbs::render('usuarios') }}
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-8">
              <h3 class="mb-0">LISTA DE USUARIOS</h3>
              <p class="text-sm mb-0">
                En esta sección puedes ver, editar, eliminar y buscar el usuario que creas conveniente.
              </p>
            </div>
            <div class="col-md-4 text-right">
              <a href="{{ route('admin.users.create') }}" class="btn btn-sm btn-default">Crear</a>
            </div>
          </div>
        </div>
        <div class="table-responsive py-4">
          <table class="table table-flush" id="myTable">
            <thead class="thead-light">
              <tr>
                <th>ID</th>
                <th>Avatar</th>
                <th>Nombre Completo</th>
                <th>Nombre de usuario</th>
                <th>Correo</th>
                <th>Roles</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>ID</th>
                <th>Avatar</th>
                <th>Nombre Completo</th>
                <th>Nombre de usuario</th>
                <th>Correo</th>
                <th>Roles</th>
                <th>Acciones</th>
              </tr>
            </tfoot>
            <tbody>
            @foreach ($users as $user)
              <tr>
                <td class="align-middle">{{ $user->id }}</td>
                <td class="align-middle">
                  @if($user->avatar != null)
                    <img height="50px" width="50px" class="rounded-circle shadow-lg" alt="{{ $user->name }}" src="/storage/{{ $user->avatar }}" height="50">
                  @else
                    <img class="rounded-circle shadow-lg"  alt="{{ $user->name }}" src="{{ asset('assets/img/placeholder.jpg') }}" height="50">
                  @endif
                </td>
                <td class="align-middle">{{ $user->name}}</td>
                <td class="align-middle">{{ $user->username}}</td>
                <td class="align-middle">{{ $user->email }}</td>
                <td class="align-middle">{{ $user->getRoleDisplayNames() }}</td>
                <td class="align-middle">
                    <a href="{{ route('admin.users.show', $user) }}" class="btn btn-info rounded-circle btn-acciones">
                      <i class="fas fa-eye fa-sm"></i>
                    </a>

                    <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-success rounded-circle btn-acciones">
                      <i class="fas fa-pencil-alt fa-sm"></i>
                    </a>
                    <form action="{{ route('admin.users.destroy', $user) }}" method="POST" style="display: inline">
                      @csrf @method('DELETE')
                      <button class="btn btn-danger rounded-circle btn-acciones" id="deleteButton">
                        <i class="fas fa-trash-alt fa-sm"></i>
                      </button>
                    </form>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
  @include('admin.partials.footer')
</div>
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush
@push('js')
  <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "language": @include('admin.partials.datatable_es')
        });    

        $('button#deleteButton').on('click', function(e){
        e.preventDefault();
        // iniciar
        Swal.fire({
          title: '¿Estás seguro?',
          text: "¡No podrás revertir esto!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#11cdef',
          cancelButtonColor: '#f5365c',
          confirmButtonText: 'Si, eliminar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.value) {
              Swal.fire(
                'Eliminado',
                'Registro eliminado con éxito.',
                'success'
              )
              $(this).closest("form").submit();
            }
          }) 
        });
    });
</script>
@endpush