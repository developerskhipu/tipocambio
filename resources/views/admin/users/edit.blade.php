@extends('layouts.app')
@section('title', 'Editar usuario')
@section('content')
{{ Breadcrumbs::render('usuarios.editar', $user) }}
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Editar Perfil</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if($errors->any())
                            <ul class="list-group">
                                @foreach($errors->all() as $error)
                                    <li class="py-1 h5 list-group-item bg-warning text-secondary">{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h6 class="heading-small text-muted mb-4">Información del Usuario</h6>
                        <form method="POST" action="{{ route('admin.users.update', $user) }}" enctype="multipart/form-data">
                            @csrf @method('PUT')
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="avatar">Avatar</label>
                                            <input type="file" name="avatar" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">Nombre</label>
                                            <input name="name" class="form-control" placeholder="Nombre" value="{{ old('name', $user->name) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">Email</label>
                                            <input name="email" class="form-control" placeholder="hola@email.com" value="{{ old('email', $user->email) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="username">Nombre de usuario</label>
                                            <input name="username" class="form-control" placeholder="Nombre de usuario" value="{{ old('username', $user->username) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="password">Contraseña</label>
                                            <input type="password" name="password" class="form-control" placeholder="Contraseña">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="password_confirmation">Repite la contraseña</label>
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Repite la contraseña">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 mr-auto ml-auto">
                                        <div class="form-group">
                                            <button class="btn btn-default btn-block">Actualizar Perfil</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h3 class="mb-0">Roles</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                @role('admin')
                                <form method="POST" action="{{ route('admin.users.roles.update', $user) }}">
                                    @csrf @method('PUT')
                                    @include('admin.roles.checkboxes')
                                    <button class="mt-3 btn btn-default btn-block">Actualizar Roles</button>
                                </form>
                                @else
                                    <ul class="list-group">
                                        @forelse ($user->roles as $role)
                                            <li class="list-group-item">{{ $role->name }}</li>
                                        @empty
                                            <li class="list-group-item">No tienes roles</li>
                                        @endforelse
                                    </ul>
                                    @endrole
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h3 class="mb-0">Permisos</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                @role('admin')
                                <form method="POST" action="{{ route('admin.users.permissions.update', $user) }}">
                                    @csrf @method('PUT')
                                    @include('admin.permissions.checkboxes', ['model' => $user])
                                    <button class="mt-3 btn btn-default btn-block">Actualizar Permisos</button>
                                </form>
                                @else
                                    <ul class="list-group">
                                        @forelse ($user->permissions as $permission)
                                            <li class="list-group-item">{{ $permission->name }}</li>
                                        @empty
                                            <li class="list-group-item">No tienes permisos</li>
                                        @endforelse
                                    </ul>
                                    @endrole
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection