@extends('layouts.app')
@section('title', 'Listado de Permisos')
@section('content')
{{ Breadcrumbs::render('permisos') }}
  <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-md-8">
                  <h3 class="mb-0">LISTA DE PERMISOS</h3>
                  <p class="text-sm mb-0">
                    En esta sección puedes ver, editar, eliminar y buscar los permisos que creas conveniente.
                  </p>
                </div>
              </div>
            </div>
            <div class=" table-responsive py-4">
              <table class="table table-flush" id="myTable">
                <thead class="thead-light">
                  <tr>
                    <th>ID</th>
                    <th>Identificador</th>
                    <th>Nombre</th>
                    <th>Guard</th>
                    <th>Acciones</th>  
                  </tr>
                </thead>
                <tbody>
                  @foreach ($permissions as $permission)
                  <tr>
                    <td class="align-middle">{{ $permission->id }}</td>
                    <td class="align-middle">{{ $permission->name}}</td>     
                    <td class="align-middle">{{ $permission->display_name}}</td>      
                    <td class="align-middle">{{ $permission->guard_name }}</td>
                    <td class="align-middle">
                      <a href="{{ route('admin.permissions.edit', $permission) }}" class="btn btn-success rounded-circle btn-acciones">
                        <i class="fas fa-pencil-alt fa-sm"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
      @include('admin.partials.footer')
  </div>
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush
@push('js')
  <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "language": @include('admin.partials.datatable_es')
        });    

        $('button#deleteButton').on('click', function(e){
        e.preventDefault();
        // iniciar
        Swal.fire({
          title: '¿Estás seguro?',
          text: "¡No podrás revertir esto!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#11cdef',
          cancelButtonColor: '#f5365c',
          confirmButtonText: 'Si, eliminar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.value) {
              Swal.fire(
                'Eliminado',
                'Registro eliminado con éxito.',
                'success'
              )
              $(this).closest("form").submit();
            }
          }) 
        });
    });
</script>
@endpush