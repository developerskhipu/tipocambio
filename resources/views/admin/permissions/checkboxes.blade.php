@foreach($permissions as $id => $name)
    <div class="form-check">
        <input name="permissions[]" type="checkbox" value="{{ $name }}" 
            id="checkPermission{{ $id }}" class="form-check-input" 
            {{ $model->permissions->contains($id) || collect(old('permissions'))->contains($name) ? 'checked' : ''}}>            
        <label class="form-check-label" for="checkPermission{{ $id }}">{{ $name }}</label>
    </div>
@endforeach