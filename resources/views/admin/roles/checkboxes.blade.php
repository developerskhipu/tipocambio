@foreach($roles as $role)
    <div class="form-check">
        <input name="roles[]" value="{{ $role->name }}" id="checkRol{{ $role->id }}" type="checkbox" class="form-check-input" {{ $user->roles->contains($role->id) ? 'checked' : ''}}>
        <label class="form-check-label" for="checkRol{{ $role->id }}">{{ $role->name }}</label>
        <br>
        <small class="text-muted">{{ $role->permissions->pluck('name')->implode(', ') }}</small>
    </div>
@endforeach