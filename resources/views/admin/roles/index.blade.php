@extends('layouts.app')
@section('title', 'Listado de Roles')
@section('content')
{{ Breadcrumbs::render('roles') }}
  <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-md-8">
                  <h3 class="mb-0">LISTA DE ROLES</h3>
                  <p class="text-sm mb-0">
                    En esta sección puedes ver, editar, eliminar y buscar los roles que creas conveniente.
                  </p>
                </div>
                <div class="col-md-4 text-right">
                  <a href="{{ route('admin.roles.create') }}" class="btn btn-sm btn-default">Crear</a>
                </div>
              </div>              
            </div>
            <div class=" table-responsive py-4">
              <table class="table table-flush" id="myTable">
                <thead class="thead-light">
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Identificador</th>
                    <th>Permisos</th>
                    <th>Guard</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($roles as $role)
                  <tr>
                    <td class="align-middle">{{ $role->id }}</td>
                    <td class="align-middle">{{ $role->name}}</td>     
                    <td class="align-middle">{{ $role->display_name}}</td>
                    <td class="align-middle text-wrap" style="width: 17rem;">
                        {{ $role->permissions->pluck('name')->implode(', ')}}                                          
                    </td>            
                    <td class="align-middle">{{ $role->guard_name }}</td>
                    <td class="align-middle">
                        <a href="{{ route('admin.roles.edit', $role) }}" class="btn btn-info rounded-circle" style="padding-top: 5px;padding-bottom: 5px;padding-right: 10px;padding-left: 10px;">
                            <i class="fas fa-pencil-alt fa-sm"></i>
                        </a>
                        @if($role->id !== 1)
                        <form action="{{ route('admin.roles.destroy', $role) }}" method="POST" style="display: inline">
                            @csrf @method('DELETE')
                            <button class="btn btn-danger rounded-circle" id="deleteButton" style="padding-top: 5px;padding-bottom: 5px;padding-right: 10px;padding-left: 10px;">
                                <i class="fas fa-trash-alt fa-sm"></i>
                            </button>
                        </form>
                        @endif
                    </td>
                  </tr>
                  @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
      @include('admin.partials.footer')
  </div>
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush
@push('js')
  <script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "language": @include('admin.partials.datatable_es')
        });    

        $('button#deleteButton').on('click', function(e){
        e.preventDefault();
        // iniciar
        Swal.fire({
          title: '¿Estás seguro?',
          text: "¡No podrás revertir esto!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#11cdef',
          cancelButtonColor: '#f5365c',
          confirmButtonText: 'Si, eliminar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.value) {
              Swal.fire(
                'Eliminado',
                'Registro eliminado con éxito.',
                'success'
              )
              $(this).closest("form").submit();
            }
          }) 
        });
    });
</script>
@endpush