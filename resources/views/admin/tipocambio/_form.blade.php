<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <label class="form-control-label" for="compra">Precio de compra</label>
            <input name="compra" class="form-control" placeholder="Ingresar precio de compra" type="text" value="{{ old('compra', $tipocambio->compra) }}">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <label class="form-control-label" for="venta">Precio de venta</label>
            <input name="venta" class="form-control" placeholder="Ingresar precio de venta" type="text" value="{{ old('venta', $tipocambio->venta) }}">
        </div>
    </div>
</div>
