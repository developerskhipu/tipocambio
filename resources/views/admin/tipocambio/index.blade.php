@extends('layouts.app')
@section('title', 'Listado')
@section('content')
{{ Breadcrumbs::render('tipocambios') }}
  <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-8">
                  <h2 class="mb-0">Listado</h2>
                  <p class="text-sm mb-0">
                </div>
                @if(Auth::user()->hasPermissionTo('crear-tipo-cambio') || Auth::user()->hasRole('admin'))
                <div class="col-4 text-right">
                  <a class="btn btn-default text-white" href="{{ route('admin.tipocambio.create')}}">Nuevo registro</a>
                </div>
                @endif
              </div>
            </div>
            <div class="table-responsive py-4">
              <table id="table-posts" class="table table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>ID</th>
                    <th>Precio Compra</th>
                    <th>Precio Venta</th>
                    <th>Fecha</th>
                    @if( Auth::user()->hasAnyPermission(['editar-tipo-cambio']) || Auth::user()->hasRole('admin'))
                      <th>Acciones</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($tipoCambios as $tipocambio)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $tipocambio->compra }}</td>
                    <td>{{ $tipocambio->venta }}</td>
                    <td>{{ $tipocambio->created_at }}</td>
                    <td>
                      @if( Auth::user()->hasPermissionTo('actualizar-tipo-cambio') || Auth::user()->hasRole('admin'))
                        <a href="{{ route('admin.tipocambio.edit', $tipocambio->id) }}" class="btn btn-info rounded-circle" style="padding-top: 5px;padding-bottom: 5px;padding-right: 10px;padding-left: 10px;">
                          <i class="fas fa-pencil-alt fa-sm"></i>
                        </a>
                      @endif
                      @if( Auth::user()->hasPermissionTo('eliminar-tipo-cambio') || Auth::user()->hasRole('admin'))
                        <form action="{{ route('admin.tipocambio.destroy', $tipocambio) }}" method="POST" style="display: inline">
                          @csrf @method('DELETE')
                          <button class="btn btn-danger rounded-circle" id="deleteButton" style="padding-top: 5px;padding-bottom: 5px;padding-right: 10px;padding-left: 10px;">
                            <i class="fas fa-trash-alt fa-sm"></i>
                          </button>
                        </form>
                      @endif
                    </td>
                  </tr>
                  @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
      @include('admin.partials.footer')
  </div>
@endsection
@push('css')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endpush
@push('js')
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="{{ asset('js/data-dataTable.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.17.6/dist/sweetalert2.all.min.js"></script>
  <script>
    $('button#deleteButton').on('click', function(e){
      e.preventDefault();
      // iniciar
      Swal.fire({
        title: '¿Estás seguro?',
        text: "¡No podrás revertir esto!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#11cdef',
        cancelButtonColor: '#f5365c',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Eliminado',
            'Su archivo ha sido eliminado.',
            'success'
          )
          $(this).closest("form").submit();
        }
      })
      // fin
    });
  </script>
@endpush
