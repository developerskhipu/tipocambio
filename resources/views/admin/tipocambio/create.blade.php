@extends('layouts.app')
@section('title', 'Crear tipo de cambio')
@section('content')
{{ Breadcrumbs::render('tipocambios.crear') }}
<div class="container-fluid mt--6">
        <div class="row">
            <div class="col-md-8 mr-auto ml-auto">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Crear Tipo de cambio</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.tipocambio.store') }}">
                            @csrf @method('POST')
                            <div class="pl-lg-4">
                                @include('admin.tipocambio._form')
                                <div class="row my-4">
                                    <div class="col-lg-6 mr-auto ml-auto">
                                        <div class="form-group">
                                            <button class="btn btn-default btn-block">GRABAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection
