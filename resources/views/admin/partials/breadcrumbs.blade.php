<div class="header bg-info pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              @if (count($breadcrumbs))
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    @foreach ($breadcrumbs as $breadcrumb)
                        @if ($breadcrumb->url && !$loop->last)
                            <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}"><i class="fas {{ $breadcrumb->icon }}"></i> {{ $breadcrumb->title }}</a></li>
                        @else
                            <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumb->title }}</li>
                        @endif
                    @endforeach
                </ol>
            @endif
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
