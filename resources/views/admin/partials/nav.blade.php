<nav class="navbar navbar-top navbar-expand navbar-dark bg-info border-bottom">
    <div class="container-fluid">
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h3 class="text-white">SISTEMA TIPO DE CAMBIO</h3>
        <ul class="navbar-nav align-items-center  ml-md-auto ">

          <li class="nav-item d-xl-none">
            <!-- Sidenav toggler -->
            <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
              <div class="sidenav-toggler-inner">
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
              </div>
            </div>
          </li>
        </ul>

        <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                    @if(Auth::user()->avatar != null)
                        <img class="rounded-circle shadow-lg" alt="{{ Auth::user()->name }}" src="/storage/{{ Auth::user()->avatar }}" height="36px" width="36px">
                    @else
                    <img class="mx-2 h-24 w-24 rounded-full object-cover" src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}&color=ffffff&background=86dc23&size=80" alt="{{ Auth::user()->name }}">
                    @endif
                </span>
                <div class="media-body  ml-2  d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->name }}</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu  dropdown-menu-right ">
              <div class="dropdown-header noti-title">
                <h6 class="text-overflow m-0">Bienvenido</h6>
              </div>
              <a href="{{ route('admin.users.show', auth()->user()) }}" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>Mi perfil</span>
              </a>
              @if(Auth::user()->hasRole('admin'))
              <a href="{{ route('admin.roles.index') }}" class="dropdown-item">
                <i class="fas fa-gem"></i>
                <span>Roles</span>
              </a>
              <a href="{{ route('admin.permissions.index') }}" class="dropdown-item">
                <i class="fas fa-key"></i>
                <span>Permisos</span>
              </a>
              @endif
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="ni ni-user-run"></i>
                <span>Desconectar</span>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
