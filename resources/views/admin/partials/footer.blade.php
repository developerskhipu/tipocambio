<footer class="footer pt-0">
    <div class="row justify-content-center">
      <div class="col-lg-12">
        <div class="copyright text-center text-muted">
          &copy; 2021 <a href="#" class="font-weight-bold ml-1 text-info" target="_blank">Desarrollado</a>
        </div>
      </div>
    </div>
</footer>
