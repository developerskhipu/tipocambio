@if(Session::has('success'))
    <script type="text/javascript">
        swal({
            title:'Felicitaciones!',
            text:"{{Session::get('success')}}",
            timer:5000,
            type:'success'
        }).then((value) => {
        }).catch(swal.noop);
    </script>
@endif