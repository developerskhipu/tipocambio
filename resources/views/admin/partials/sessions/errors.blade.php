@if($errors->any())   
    <script>
        $(document).ready(function() {
            toastr.options = {
                'closeButton': true,
                'progressBar': true,
                'positionClass': 'toast-bottom-right',
                'timeOut': '15000',
                'showEasing': 'swing',
                'hideEasing': 'linear',
                'showMethod': 'fadeIn',
                'hideMethod': 'fadeOut',
            }                
        @foreach($errors->all() as $error)
            toastr.error('<small>{{ $error }}</small>', '¡Error!');
        @endforeach    
        });
    </script>
@endif