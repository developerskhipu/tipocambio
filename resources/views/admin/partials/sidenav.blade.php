<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <div class="sidenav-header  d-flex  align-items-center">
        <a class="navbar-brand" href="{{ url('admin') }}">
          LOGO
        </a>
        <div class=" ml-auto ">
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <hr class="my-3">
          <h6 class="navbar-heading p-0 text-muted">MÓDULO</h6>
          <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link {{ activarLink('admin.tipocambio.index') }}" href="{{ route('admin.tipocambio.index') }}" >
                  <i class="fas fa-dollar-sign"></i>
                  <span class="nav-link-text">Tipo de cambio</span>
                </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
