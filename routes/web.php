<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function(){
    return view('auth.login');
});

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\RolesController;
use App\Http\Controllers\Admin\PermissionsController;
use App\Http\Controllers\Admin\UsersRolesController;
use App\Http\Controllers\Admin\UsersPermissionsController;
use App\Http\Controllers\Admin\TipoCambiosController;


Route::group([
    'as' => 'admin.',
    'prefix' => 'admin',
    // 'namespace' => 'Admin',
    'middleware' => 'auth'
], function(){
    Route::get('/', [TipoCambiosController::class, 'index']);
    // Usuarios
    Route::resource('users', UsersController::class);
    // // Roles
    Route::resource('roles', RolesController::class)->except(['show']);
    // // Permisos
    Route::resource('permissions', PermissionsController::class)->only(['index', 'edit', 'update']);
    // // Actualizar Roles de Usuario y Permisos de Usuario
    Route::middleware('role:admin')
        ->put('users/{user}/roles', [UsersRolesController::class, 'update'])
        ->name('users.roles.update');
    Route::middleware('role:admin')
        ->put('users/{user}/permissions', [UsersPermissionsController::class, 'update'])
        ->name('users.permissions.update');
    /********************** INICIO MÓDULOS *********************/
    Route::resource('tipocambio', TipoCambiosController::class)->except(['show']);
    /**********************   FIN MÓDULOS  *********************/
});
