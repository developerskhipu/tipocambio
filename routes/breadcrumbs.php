<?php
// Admin
Breadcrumbs::for('admin', function ($trail) {
    $trail->push('Inicio', route('admin.'), ['icon' => 'fa-home']);
});
/* --------------------------------------------------------------------------- */
// Admin > Usuarios
Breadcrumbs::for('usuarios', function ($trail) {
    $trail->parent('admin');
    $trail->push('Usuarios', route('admin.users.index'), ['icon' => '']);
});
// Admin > Usuarios > Crear
Breadcrumbs::for('usuarios.crear', function ($trail) {
    $trail->parent('usuarios');
    $trail->push('Crear', route('admin.users.create'));
});
// Admin > Usuarios > Editar
Breadcrumbs::for('usuarios.editar', function ($trail, $user) {
    $trail->parent('usuarios');
    $trail->push("Editar", route('admin.users.edit', $user));
});
// Admin > Usuarios > Ver
Breadcrumbs::for('usuarios.ver', function ($trail, $user) {
    $trail->parent('usuarios');
    $trail->push("Ver", route('admin.users.show', $user));
});
/* --------------------------------------------------------------------------- */
// Admin > Roles
Breadcrumbs::for('roles', function ($trail) {
    $trail->parent('admin');
    $trail->push('Roles', route('admin.roles.index'), ['icon' => '']);
});
// Admin > Roles > Crear
Breadcrumbs::for('roles.crear', function ($trail) {
    $trail->parent('roles');
    $trail->push('Crear', route('admin.roles.create'));
});
// Admin > Roles > Editar
Breadcrumbs::for('roles.editar', function ($trail, $role) {
    $trail->parent('roles');
    $trail->push("Editar", route('admin.roles.edit', $role));
});
/* --------------------------------------------------------------------------- */
// Admin > Permisos
Breadcrumbs::for('permisos', function ($trail) {
    $trail->parent('admin');
    $trail->push('Permisos', route('admin.permissions.index'), ['icon' => '']);
});
// Admin > Permisos > Editar
Breadcrumbs::for('permisos.editar', function ($trail, $permission) {
    $trail->parent('permisos');
    $trail->push("Editar", route('admin.permissions.edit', $permission));
});
/* --------------------------------------------------------------------------- */
// Admin > Categorias
Breadcrumbs::for('tipocambios', function ($trail) {
    $trail->parent('admin');
    $trail->push('Tipo de cambios', route('admin.tipocambio.index'), ['icon' => '']);
});
// Admin > Categorias > Crear
Breadcrumbs::for('tipocambios.crear', function ($trail) {
    $trail->parent('tipocambios');
    $trail->push('Crear', route('admin.tipocambio.create'));
});
// Admin > Categorias > Editar
Breadcrumbs::for('tipocambios.editar', function ($trail, $tipocambio) {
    $trail->parent('tipocambios');
    $trail->push("Editar", route('admin.tipocambio.edit', $tipocambio));
});
