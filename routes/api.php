<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TipoCambiosController;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api_key')->get('tipocambios/AbCd123', [TipoCambiosController::class, 'index'])->name('tipocambio.listar');

Route::middleware('api_key')->get('tipocambio/AbCd123', [TipoCambiosController::class, 'ultimo'])->name('tipocambio.ultimo');
