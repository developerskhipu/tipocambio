<?php

namespace App\Providers;
use App\Models\TipoCambio;
use App\Observers\TipoCambioObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::resourceVerbs([
            'create' => 'crear',
            'edit'  => 'editar'
        ]);
        Paginator::useBootstrap();
        TipoCambio::observe(TipoCambioObserver::class);
    }
}
