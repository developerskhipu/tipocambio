<?php

namespace App\Observers;

use App\Models\TipoCambio;
use Illuminate\Support\Facades\Auth;

class TipoCambioObserver
{
    /**
     * Handle the TipoCambio "created" event.
     *
     * @param  \App\Models\TipoCambio  $tipoCambio
     * @return void
     */
    public function created(TipoCambio $tipoCambio)
    {
        //
    }

    /**
     * Handle the TipoCambio "updated" event.
     *
     * @param  \App\Models\TipoCambio  $tipoCambio
     * @return void
     */
    public function updated(TipoCambio $tipoCambio)
    {
        //
    }

    /**
     * Handle the TipoCambio "deleted" event.
     *
     * @param  \App\Models\TipoCambio  $tipoCambio
     * @return void
     */
    public function deleted(TipoCambio $tipoCambio)
    {
        $tipoCambio->deleted_by = Auth::user()->id;
        $tipoCambio->save();
    }

    /**
     * Handle the TipoCambio "restored" event.
     *
     * @param  \App\Models\TipoCambio  $tipoCambio
     * @return void
     */
    public function restored(TipoCambio $tipoCambio)
    {
        //
    }

    /**
     * Handle the TipoCambio "force deleted" event.
     *
     * @param  \App\Models\TipoCambio  $tipoCambio
     * @return void
     */
    public function forceDeleted(TipoCambio $tipoCambio)
    {
        //
    }
}
