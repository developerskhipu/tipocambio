<?php

namespace App\Policies;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        return $user->hasRole('admin') ? true : null;
    }

    public function view(User $user, Role $role)
    {
        return $user->hasRole('admin') || $user->hasPermissionTo('ver-role');
    }

    public function create(User $user)
    {
        return $user->hasRole('admin') || $user->hasPermissionTo('crear-role');
    }

    public function update(User $user, Role $role)
    {
        return $user->hasRole('admin') || $user->hasPermissionTo('actualizar-role');
    }

    public function delete(User $user, Role $role)
    {
        if( $role->id === 1){
            return false;
        }
        return $user->hasRole('admin') || $user->hasPermissionTo('eliminar-role');
    }
}
