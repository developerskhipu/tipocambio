<?php

namespace App\Policies;

use App\Models\TipoCambio;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TipoCambioPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if( $user->hasRole('admin')){
            return true;
        }
    }

    public function viewAny(User $user)
    {
        //
    }

    public function view(User $user, TipoCambio $tipoCambio)
    {
        return $user->id === $tipoCambio->user_id || $user->hasPermissionTo('ver-tipo-cambio');
    }

    public function create(User $user)
    {
        return $user->hasPermissionTo('crear-tipo-cambio');
    }

    public function update(User $user, TipoCambio $tipoCambio)
    {
        return $user->id === $tipoCambio->user_id || $user->hasPermissionTo('editar-tipo-cambio');
    }

    public function delete(User $user, TipoCambio $tipoCambio)
    {
        return $user->id === $tipoCambio->user_id || $user->hasPermissionTo('eliminar-tipo-cambio');
    }

    public function restore(User $user, TipoCambio $tipoCambio)
    {
        //
    }

    public function forceDelete(User $user, TipoCambio $tipoCambio)
    {
        //
    }
}
