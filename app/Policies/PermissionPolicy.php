<?php

namespace App\Policies;

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        return $user->hasRole('admin') ? true : null;
    }

    public function view(User $user, Permission $permission)
    {
        return $user->hasRole('admin') || $user->hasPermissionTo('ver-permiso');
    }

    public function update(User $user, Permission $permission)
    {
        return $user->hasRole('admin') || $user->hasPermissionTo('actualizar-permiso');
    }
}
