<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidarApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $key = $request->route()->parameters();
        if($key = 'AbCd123'){
            return $next($request);
        }
    }
}
