<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoCambioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST'){
            return [
                'compra'   => 'required|numeric',
                'venta'   => 'required|numeric'
            ];
        }

        if ($this->method() == 'PUT'){
            return [
                'compra'   => 'required|numeric',
                'venta'   => 'required|numeric'
            ];
        }
    }

    public function attributes()
    {
        return [
            'compra'         => 'precio de compra',
            'venta'           => 'precio de venta'
        ];
    }
}
