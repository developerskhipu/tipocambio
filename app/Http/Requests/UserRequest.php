<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore($this->route('user'))
            ],
            'avatar' => [
                $this->route('users') ? 'nullable' : '',
                'mimes:jpeg,png',
                'max:2048'
            ],
            'username' =>  'required|max:20',
        ];

        if($this->filled('password')){
            $rules['password'] = ['confirmed', 'min:6'];
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name'      => 'nombre',
            'email'     => 'correo',
            'password'  => 'contraseña',
            'username'  => 'nombre de usuario',
        ];
    }
}
