<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TipoCambio;
use Illuminate\Http\Request;
use App\Http\Requests\TipoCambioRequest;

class TipoCambiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', new TipoCambio);
        $tipoCambios = TipoCambio::orderBy('created_at', 'DESC')->get();
        return view('admin.tipocambio.index', compact('tipoCambios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', new TipoCambio);
        $tipocambio = new TipoCambio;
        return view('admin.tipocambio.create', compact('tipocambio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoCambioRequest $request)
    {
        $this->authorize('create', new TipoCambio);
        $tipoCambio = new TipoCambio;
        $tipoCambio->compra = $request->compra;
        $tipoCambio->venta = $request->venta;
        $tipoCambio->user_id = auth()->id();
        $tipoCambio->save();
        return redirect()->route('admin.tipocambio.index')->with('success', 'Tipo de cambio creado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoCambio $tipocambio_id, $id)
    {
        $this->authorize('update', $tipocambio_id);
        $tipocambio = TipoCambio::find($id);
        return view('admin.tipocambio.edit', compact('tipocambio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipoCambio $tipocambio, TipoCambioRequest $request)
    {
        $this->authorize('update', $tipocambio);
        $tipocambio->update( $request->validated() );
        return redirect()->route('admin.tipocambio.index')->with('success','Tipo de cambio actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoCambio $tipocambio)
    {
        $this->authorize('delete', $tipocambio);
        $tipocambio->delete();
        return redirect()->route('admin.tipocambio.index')->with('success', 'Tipo de cambio eliminado con éxito');
    }
}
