<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UserRequest;
use App\Models\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::allowed()->get();
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $user = new User;
        $this->authorize('create', $user);
        $roles = Role::with('permissions')->get();
        $permissions = Permission::pluck('name', 'id');
        return view('admin.users.create', compact('user','roles', 'permissions'));
    }

    public function store(UserRequest $request)
    {
        $this->authorize('create', new User);
        $user = User::create($request->validated());
        if($request->filled('roles')){
            $user->assignRole($request->roles);
        }
        if($request->filled('permissions')){
            $user->givePermissionTo($request->permissions);
        }
       return redirect()->route('admin.users.index')->with('success','El usuario ha sido creado');
    }

    public function show(User $user)
    {
        $this->authorize('view', $user);
        return view('admin.users.show', compact('user'));
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);
        $roles = Role::with('permissions')->get();
        $permissions = Permission::pluck('name', 'id');
        return view('admin.users.edit', compact('user', 'roles', 'permissions'));
    }

    public function update(User $user, UserRequest $request){
        $this->authorize('update', $user);
        if( $request->hasFile('avatar') ){
            Storage::delete($user->avatar);
            $user->fill( $request->validated() );
            $user->avatar = $request->file('avatar')->store('img/usuario');
            $user->save();
        } else {
            $user->update( array_filter($request->validated()) );
        }
        return redirect()->route('admin.users.edit', $user)->with('success','Usuario actualizado');
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', $user);
        Storage::delete($user->avatar);
        $user->delete();
        return redirect()->route('admin.users.index')->with('success','Usuario eliminado');
    }
}
