<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TipoCambio;
use Illuminate\Http\Request;

class TipoCambiosController extends Controller
{
    public function index()
    {
        $tipoCambios = TipoCambio::select('venta', 'compra')->orderBy('created_at', 'DESC')->get();
        return response()->json(['data' => $tipoCambios], 200);
    }


    public function ultimo()
    {
        $tipoCambios = TipoCambio::select('venta', 'compra')->orderBy('created_at', 'DESC')->limit(1)->get();
        return response()->json(['data' => $tipoCambios], 200);
    }


}
