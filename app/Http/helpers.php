<?php
function activarLink($ruta){
    return request()->routeIs($ruta) ? 'active' : '';
}

function activarExpandir($ruta){
    return request()->routeIs($ruta) ? 'true' :'false';
}

function activarCollapsar($ruta){
    return request()->routeIs($ruta) ? 'show' : '';
}
