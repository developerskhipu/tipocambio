<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoCambio extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['compra', 'venta', 'user_id', 'deleted_by'];

    public function usuario(){
        return $this->belongsTo(User::class);
    }

    public function scopeAllowed($query)
    {
        if( auth()->user()->can('view', $this)){
            return $query;
        }
        return $query->where('id', auth()->id());
    }
}
