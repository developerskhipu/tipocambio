<?php

return [
    'tipo_documento'=> [
        'DIRECTIVA',
        'RESOLUCION',
        'GESTION',
        'PTE'
    ],
    'sucursal' => [
        1 => 'JAEN',
        2 => 'BELLAVISTA',
        3 => 'SAN IGNACIO'
    ]
];
